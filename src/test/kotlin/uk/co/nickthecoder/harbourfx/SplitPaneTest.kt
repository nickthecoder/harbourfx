package uk.co.nickthecoder.harbourfx

import javafx.application.Application
import javafx.beans.property.SimpleDoubleProperty
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.control.SplitPane
import javafx.scene.layout.BorderPane
import javafx.stage.Stage

class SplitPaneTest : Application() {

    override fun start(primaryStage: Stage) {
        //val node = testRegular()
        testTriple(primaryStage)
    }

    private fun testRegular(stage: Stage): Parent {
        val splitPane = SplitPane()
        val left = ScrollPane(Label("Hello"))
        val right = ScrollPane(Label("World"))

        val scene = Scene(splitPane, 300.0, 300.0)
        stage.scene = scene
        stage.show()

        splitPane.items.addAll(left, right)
        splitPane.setDividerPosition(0, 0.3)
        SplitPane.setResizableWithParent(left, false)
        return splitPane
    }

    private fun testTriple(stage: Stage): Parent {

        val splitPos = SimpleDoubleProperty(0.3)
        val left = ScrollPane(Label("Hello 3"))
        val right = ScrollPane(Label("World 3"))
        val splitPane = TripleSplitPane(right)

        val borderPane = BorderPane()
        borderPane.top = Label("Top")
        borderPane.center = splitPane

        val scene = Scene(borderPane, 300.0, 300.0)
        stage.scene = scene
        stage.show()

        splitPane.left = left
        splitPane.firstDividerPosition().bindBidirectional(splitPos)
        SplitPane.setResizableWithParent(left, false)
        return splitPane
    }

    companion object {
        @JvmStatic
        fun main(vararg args: String) {
            Application.launch(SplitPaneTest::class.java, * args)
        }
    }
}
