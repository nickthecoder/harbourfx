/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.Node
import javafx.scene.control.SingleSelectionModel
import javafx.scene.control.Tab

/**
 * A [Dockable] with a set of [Tab]s, rather than a single content [Node].
 *
 * The [TabbedDock] is responsible for rendering the tabs (which appear in the [TabbedDock]'s ToolBar).
 *
 * Use [AbstractTabbedDockable] unless you have specific reasons not to!
 */
interface TabbedDockable : Dockable {

    val tabs: ObservableList<Tab>

    val selectionModel: SingleSelectionModel<Tab?>

    override fun createDock(half: DockSide.Half): Dock = TabbedDock(half, this)
}

/**
 * A default implementation of [TabbedDockable].
 */
abstract class AbstractTabbedDockable : TabbedDockable {

    abstract val noTabContent: Node

    override val tabs: ObservableList<Tab> = FXCollections.observableArrayList<Tab>()

    final override val selectionModel = object : SingleSelectionModel<Tab?>() {
        override fun getItemCount() = tabs.size
        override fun getModelItem(index: Int) = if (index < 0 || index >= tabs.size) null else tabs[index]
    }

    protected var lastSelectedTab = selectionModel.selectedItem
    protected var lastSelectedTabName = selectionModel.selectedItem?.text

    override val dockableContent = SimpleObjectProperty<Node>()

    init {
        selectionModel.selectedItemProperty().addListener { _, _, newValue ->
            dockableContent.value = newValue?.content ?: noTabContent
        }
    }

    protected fun rememberSelectedTab() {
        lastSelectedTab = selectionModel.selectedItem ?: lastSelectedTab
    }

    protected fun reselectTab() {
        if (tabs.contains(lastSelectedTab)) {
            selectionModel.select(lastSelectedTab)
        } else {
            selectionModel.select(tabs.firstOrNull { it.text == lastSelectedTabName } ?: tabs.firstOrNull())
        }
    }

    protected fun rebuild() {
        rememberSelectedTab()
        tabs.clear()
        selectionModel.select(null)

        rebuildTabs()

        reselectTab()
    }

    protected abstract fun rebuildTabs()
}
