/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.beans.binding.Bindings
import javafx.beans.property.*
import javafx.beans.value.ChangeListener
import javafx.collections.ListChangeListener
import javafx.geometry.Orientation
import javafx.scene.Node
import javafx.scene.control.Control
import javafx.scene.control.SkinBase
import javafx.scene.control.SplitPane

/**
 * A split pane, which can have one, two or three panes.
 * The divider positions are remembered, so removing and then re-adding a node will keep the divider in
 * the same place.
 *
 * The [center] node is mandatory (cannot be null), whereas [left] and [right] can be null.
 *
 * This is implemented using the regular [SplitPane] in [TripleSplitPaneSkin].
 *
 * Harbour uses two of these [HarbourSkin.topMiddleBottom] and [HarbourSkin.leftMiddleRight].
 */
class TripleSplitPane(center: Node)

    : Control() {

    private val centerProperty = SimpleObjectProperty<Node>(center)
    fun centerProperty() = centerProperty
    var center: Node
        get() = centerProperty.get()
        set(v) {
            centerProperty.value = v
        }

    private val leftProperty = SimpleObjectProperty<Node?>(null)
    fun leftProperty(): ObjectProperty<Node?> = leftProperty

    var left: Node?
        get() = leftProperty.value
        set(v) {
            leftProperty.value = v
        }

    private val rightProperty = SimpleObjectProperty<Node?>(null)
    fun rightProperty(): ObjectProperty<Node?> = rightProperty
    var right: Node?
        get() = rightProperty.value
        set(v) {
            rightProperty.value = v
        }

    /**
     * An alias for [leftProperty]
     */
    fun topProperty() = leftProperty()

    /**
     * An alias for [rightProperty]
     */
    fun bottomProperty() = rightProperty()

    /**
     * An alias for [left] - useful when orientation is VERTICAL
     */
    var top: Node?
        get() = left
        set(v) {
            left = v
        }

    /**
     * An alias for [right] - useful when orientation is VERTICAL
     */
    var bottom: Node?
        get() = right
        set(v) {
            right = v
        }

    private val firstDividerPositionProperty = SimpleDoubleProperty(0.3)
    fun firstDividerPosition(): DoubleProperty = firstDividerPositionProperty
    var firstDividerPosition: Double
        get() = firstDividerPositionProperty.value
        set(v) {
            firstDividerPositionProperty.value = v
        }

    private val lastDividerPositionProperty = SimpleDoubleProperty(0.7)
    fun lastDividerPosition(): DoubleProperty = lastDividerPositionProperty
    var lastDividerPosition: Double
        get() = lastDividerPositionProperty.value
        set(v) {
            lastDividerPositionProperty.value = v
        }

    private val orientationProperty = SimpleObjectProperty<Orientation>(Orientation.HORIZONTAL)
    fun orientationProperty(): ObjectProperty<Orientation> = orientationProperty
    var orientation: Orientation
        get() = orientationProperty.value
        set(v) {
            orientationProperty.value = v
        }

    internal val itemCountProperty = SimpleIntegerProperty(0)
    fun itemCount(): ReadOnlyIntegerProperty = itemCountProperty
    val itemCount: Int
        get() = itemCount().value.toInt()


    init {
        styleClass.add("triple-split-pane")
    }

    override fun createDefaultSkin() = TripleSplitPaneSkin(this)

    fun setSideNode(isFirst: Boolean, node: Node?) {
        if (isFirst) {
            left = node
        } else {
            right = node
        }
    }

    fun getSideNode(isFirst: Boolean) = if (isFirst) left else right


    companion object {

        fun setResizableWithParent(node: Node, value: Boolean) {
            SplitPane.setResizableWithParent(node, value)
        }

        fun isResizableWithParent(node: Node): Boolean = SplitPane.isResizableWithParent(node)
    }

}

class TripleSplitPaneSkin(control: TripleSplitPane)

    : SkinBase<TripleSplitPane>(control) {

    internal val splitPane = SplitPane()

    private val leftDivPosListener = ChangeListener<Number> { _, _, newValue ->
        control.firstDividerPosition = newValue.toDouble()
    }
    private val rightDivPosListener = ChangeListener<Number> { _, _, newValue ->
        control.lastDividerPosition = newValue.toDouble()
    }

    init {
        splitPane.dividers.addListener { c: ListChangeListener.Change<out SplitPane.Divider> ->
            while (c.next()) {
                c.addedSubList.forEach {
                    if (it == splitPane.dividers[0] && splitPane.items.firstOrNull() == control.left) {
                        it.positionProperty().addListener(leftDivPosListener)
                    } else {
                        it.positionProperty().addListener(rightDivPosListener)
                    }
                }

                c.removed.forEach {
                    it.positionProperty().removeListener(leftDivPosListener)
                    it.positionProperty().removeListener(rightDivPosListener)
                }
            }
        }

        children.add(splitPane)
        control.left?.let { splitPane.items.add(it) }
        splitPane.items.add(control.center)
        control.right?.let { splitPane.items.add(it) }
        if (control.left != null) {
            splitPane.setDividerPosition(0, control.firstDividerPosition)
        }
        if (control.right != null) {
            splitPane.setDividerPosition(splitPane.dividerPositions.size - 1, control.lastDividerPosition)
        }
        control.firstDividerPosition().addListener { _, _, newValue ->
            if (control.left != null) {
                splitPane.setDividerPosition(0, newValue.toDouble())
            }

        }
        control.lastDividerPosition().addListener { _, _, newValue ->
            if (control.right != null) {
                splitPane.setDividerPosition(splitPane.dividerPositions.size - 1, newValue.toDouble())
            }
        }

        control.itemCountProperty.bind(Bindings.size(splitPane.items))
        splitPane.orientationProperty().bind(control.orientationProperty())

        control.leftProperty().addListener { _, oldValue, newValue ->

            val leftDiv = control.firstDividerPosition
            val rightDiv = control.lastDividerPosition

            if (oldValue != null) {
                splitPane.items.remove(oldValue)
            }
            if (newValue != null) {
                splitPane.items.add(0, newValue)
            }
            resetDivs(leftDiv, rightDiv)
        }

        control.rightProperty().addListener { _, oldValue, newValue ->

            val leftDiv = control.firstDividerPosition
            val rightDiv = control.lastDividerPosition

            if (oldValue != null) {
                splitPane.items.remove(oldValue)
            }
            if (newValue != null) {
                splitPane.items.add(newValue)
            }
            resetDivs(leftDiv, rightDiv)
        }


        control.centerProperty().addListener { _, oldValue, newValue ->
            splitPane.items[splitPane.items.indexOf(oldValue)] = newValue
        }

    }

    private fun resetDivs(left: Double, right: Double) {
        if (skinnable.left != null) {
            splitPane.setDividerPosition(0, left)
        }
        if (skinnable.right != null) {
            splitPane.setDividerPosition(splitPane.items.size - 2, right)
        }
    }

}
