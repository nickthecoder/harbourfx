/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.application.Platform
import javafx.beans.property.BooleanProperty
import javafx.beans.property.DoubleProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.geometry.Orientation
import javafx.geometry.Side
import javafx.scene.Node
import javafx.scene.control.Control
import javafx.scene.control.Skin
import javafx.scene.control.SkinBase
import javafx.scene.control.SplitPane
import javafx.scene.input.TransferMode
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import java.util.prefs.Preferences

/**
 * Expanding on the "Dock" metaphor, a Harbour has many places to dock a ship.
 *
 * This is the large scale structure of the main window. It contains everything apart from the
 * the tool bar, menu bar, status bar.
 *
 * In the center is the window's main content. For Fizzy's main window, the center is a TabPane.
 *
 * At the four edges are [DockSide]s, which display toggle buttons to hide/show their [Dockable] items.
 *
 * You may move a [Dockable] to another [DockSide] by dragging its toggle button.
 * Dragging anywhere other than a [DockSide] will detach it, forming a floating window. (Not implemented yet).
 *
 * When a [Dockable] is selected (by clicking its toggle button), the [Dockable] is made visible by
 * wrapping it in a [Dock], and placing the [Dock] between the [DockSide] and the central area.
 *
 */
open class Harbour(
        val center: Node,
        val factory: DockableFactory)

    : Control() {

    /**
     * Should the [DockSide]s resize themselves when the Harbour is resized?
     * The default value is false.
     * If you want some sides to be resizable with their parent, and others not to be, then
     * unbind them like so :
     *
     *      myHarbour.left.resizableWithParentProperty().unbind()
     *      myHarbour.left.resizableWithParent = true
     *
     * Note, the in-built persistence mechanism doesn't save this data.
     */
    var resizableWithParent: Boolean
        get() = resizableWithParentProperty.value
        set(v) {
            resizableWithParentProperty.value = v
        }
    private val resizableWithParentProperty = SimpleBooleanProperty(false)
    fun resizableWithParentProperty(): BooleanProperty = resizableWithParentProperty

    val left = DockSide(this, Side.LEFT)
    val right = DockSide(this, Side.RIGHT)
    val top = DockSide(this, Side.TOP)
    val bottom = DockSide(this, Side.BOTTOM)

    val halves = listOf(left.mainHalf, left.otherHalf, right.mainHalf, right.otherHalf, top.mainHalf, top.otherHalf, bottom.mainHalf, bottom.otherHalf)

    val floatings: ObservableList<Floating> = FXCollections.observableArrayList<Floating>()

    /**
     * When false, if a Dockable is floated, and then closed, it will magically re-appear in [left].mainHalf
     */
    var allowClose: Boolean = true

    /**
     * Along with [defaultIsMain], this determines where Dockables are placed, when [addDockable] is called without
     * a Side specified.
     */
    var defaultSide: Side? = Side.LEFT

    var defaultIsMain = true


    init {
        styleClass.add("harbour")

        floatings.addListener { c: ListChangeListener.Change<out Floating> ->
            if (!allowClose) {
                while (c.next()) {
                    c.removed.forEach { floating ->
                        floating.docks().forEach { dock ->
                            val dockable = dock.dockable
                            floating.remove(dockable)
                            left.mainHalf.dockables.add(dockable)
                        }
                    }
                }
            }
        }
    }


    override fun createDefaultSkin(): Skin<Harbour> = HarbourSkin(this)

    fun clear() {
        halves.forEach { it.dockables.clear() }
    }

    /**
     * Looks for an existing Dockable, and if there isn't one, then uses [factory] to create one.
     * If the factory cannot create one, then null is returned.
     */
    fun findOrCreatedDockable(dockableId: String): Dockable? {
        return findDockable(dockableId) ?: factory.createDockable(dockableId)
    }

    /**
     * Looks for an existing Dockable within the Harbour with the given [dockableId].
     * @return null if no Dockable with that id is in the habour.
     */
    fun findDockable(dockableId: String) = find(dockableId)?.second

    fun find(dockableId: String): Pair<DockOwner, Dockable>? {
        for (side in listOf(left, right, top, bottom)) {
            for (half in listOf(side.mainHalf, side.otherHalf)) {
                for (dockable in half.dockables) {
                    if (dockable.dockableId == dockableId) {
                        return Pair(half, dockable)
                    }
                }
            }
        }
        for (floating in floatings) {
            for (dock in floating.docks()) {
                if (dock.dockable.dockableId == dockableId) {
                    return Pair(floating, dock.dockable)
                }
            }
        }
        return null
    }

    fun findOwner(dockable: Dockable): DockOwner? {
        for (half in halves) {
            if (half.dockables.contains(dockable)) return half
        }
        return null
    }

    private val dividerPositions: Map<Side, DoubleProperty> = mapOf(
            Side.LEFT to SimpleDoubleProperty(0.3),
            Side.RIGHT to SimpleDoubleProperty(0.7),
            Side.TOP to SimpleDoubleProperty(0.3),
            Side.BOTTOM to SimpleDoubleProperty(0.7)
    )

    fun dividerPositionProperty(side: Side): DoubleProperty = dividerPositions[side]!!
    fun getDividerPosition(side: Side): Double = dividerPositionProperty(side).value
    fun setDividerPosition(side: Side, value: Double) {
        dividerPositionProperty(side).value = value
    }

    private val splitPositions: Map<Side, DoubleProperty> = mapOf(
            Side.LEFT to SimpleDoubleProperty(0.5),
            Side.RIGHT to SimpleDoubleProperty(0.5),
            Side.TOP to SimpleDoubleProperty(0.5),
            Side.BOTTOM to SimpleDoubleProperty(0.5)
    )

    fun splitPositionProperty(side: Side): DoubleProperty = splitPositions[side]!!
    fun getSplitPosition(side: Side): Double = splitPositionProperty(side).value
    fun setSplitPosition(side: Side, value: Double) {
        splitPositionProperty(side).value = value
    }

    /**
     * Looks for an existing Dockable with the given dockableId.
     * If one is found, then it is made visible.
     * Otherwise use the [factory] to create a new Dockable, and show it.
     * @return false if no existing Dockable was found, and [factory] couldn't create one.
     */
    fun show(dockableId: String): Boolean {
        val found = find(dockableId)
        if (found != null) {
            found.first.show(found.second)
            return true
        }

        val dockable = factory.createDockable(dockableId) ?: return false
        show(dockable)
        return true
    }

    fun show(dockable: Dockable) {
        val half = findOwner(dockable)
        if (half == null) {
            addDockable(dockable).show(dockable)
        } else {
            half.show(dockable)
        }
    }

    fun toggle(dockableId: String): Boolean {
        val found = find(dockableId)
        if (found != null) {
            found.first.toggle(found.second)
            return true
        }

        return show(dockableId)
    }

    fun toggle(dockable: Dockable) {
        val half = findOwner(dockable)
        if (half == null) {
            addDockable(dockable)
            show(dockable)
        } else {
            half.toggle(dockable)
        }
    }

    /**
     * When [show] is called for a [Dockable] which has not been added to the [Harbour], then
     * this determines where it is placed.
     * This implementation uses [defaultSide] and [defaultIsMain] to decide where to put it.
     * However, you many override this and choose the position in other ways (maybe on a per-Dockable basis).
     */
    open fun addDockable(dockable: Dockable): DockOwner {
        if (findOwner(dockable) != null) {
            throw IllegalStateException("The dockable ${dockable.dockableId} is already in the harbour.")
        }
        val side = defaultSide
        if (side == null) {
            return makeFloating(dockable)
        } else {
            val half = getHalf(side, defaultIsMain)
            half.dockables.add(dockable)
            return half
        }
    }

    fun addDockable(dockableId: String, side: Side?, isMain: Boolean, show: Boolean): DockOwner? {
        val dockable = factory.createDockable(dockableId) ?: return null
        return addDockable(dockable, side, isMain, show)
    }

    fun addDockable(dockable: Dockable, side: Side?, isMain: Boolean, show: Boolean): DockOwner {
        return if (side == null) {
            makeFloating(dockable)
        } else {
            val owner = getHalf(side, isMain)
            owner.dockables.add(dockable)
            if (show) {
                owner.selectionModel.select(dockable)
            }
            owner
        }
    }

    fun save(preferences: Preferences) {
        preferences.node("harbour").removeNode()

        val pHarbour = preferences.node("harbour")

        for (side in Side.values()) {
            pHarbour.putDouble("divider${side.name}", getDividerPosition(side))
            pHarbour.putDouble("split${side.name}", getSplitPosition(side))
        }

        val pDockables = pHarbour.node("dockables")
        for (half in halves) {
            half.dockables.forEach { dockable ->
                val pDockable = pDockables.node(dockable.dockableId)
                pDockable.put("side", half.dockSide.side.name)
                pDockable.putBoolean("main", half.isMain())

                if (half.selectionModel.selectedItem === dockable) {
                    pDockable.putBoolean("selected", true)
                }
            }
        }

        val pFloatings = pHarbour.node("floatings")
        floatings.forEachIndexed { i, floating ->
            val pWindow = pFloatings.node(i.toString())
            pWindow.putDouble("x", floating.stage.x)
            pWindow.putDouble("y", floating.stage.y)
            pWindow.putDouble("width", floating.stage.width)
            pWindow.putDouble("height", floating.stage.height)
            val pDocks = pWindow.node("docks")
            floating.docks().forEach { dock ->
                val name = dock.dockable.dockableId
                pDocks.put(name, name)
            }
        }

        pHarbour.flush()
    }

    /**
     * Load state of the Dockables. Remember to call [save] before your application exits!
     *
     * NOTE. The harbour.scene.window must be set, but the window doesn't have to be visible yet.
     * In previous version of HarbourFX, the window must already be visible before load could
     * create [Floating] windows. This bug has now been fixed.
     */
    fun load(preferences: Preferences): Boolean {

        fun findAndRemoveDockable(id: String): Dockable? {
            find(id)?.let { (owner, dockable) ->
                owner.remove(dockable)
                return dockable
            }
            return factory.createDockable(id)
        }

        if (!preferences.nodeExists("harbour")) {
            return false
        }

        val pHarbour = preferences.node("harbour")

        for (side in Side.values()) {
            setDividerPosition(side, pHarbour.getDouble("divider${side.name}", if (side == Side.LEFT || side == Side.TOP) 0.3 else 0.7))
            setSplitPosition(side, pHarbour.getDouble("split${side.name}", 0.5))
        }

        val pDockables = pHarbour.node("dockables")
        pDockables.childrenNames().forEach { name ->
            val pDockable = pDockables.node(name)
            findAndRemoveDockable(name)?.let { dockable ->

                val isMain = pDockable.getBoolean("main", true)
                val sideStr = pDockable.get("side", "right")
                val selected = pDockable.getBoolean("selected", false)

                val side = try {
                    Side.valueOf(sideStr)
                } catch (e: Exception) {
                    Side.LEFT
                }
                val newHalf = getHalf(side, isMain)
                newHalf.dockables.add(dockable)
                if (selected) {
                    newHalf.selectionModel.select(dockable)
                }

            }
        }

        val pFloating = pHarbour.node("floatings")
        pFloating.childrenNames().forEach { name ->
            val pWindow = pFloating.node(name)

            var floating: Floating? = null
            val pDocks = pWindow.node("docks")
            pDocks.keys().forEach { key ->
                findAndRemoveDockable(key)?.let { dockable ->
                    if (floating == null) {
                        floating = makeFloating(dockable)
                    } else {
                        floating!!.add(dockable)
                    }

                }
            }
            floating?.let {
                val x = pWindow.getDouble("x", -1.0)
                val y = pWindow.getDouble("y", -1.0)
                if (x >= 0 && y >= 0) {
                    it.stage.x = x
                    it.stage.y = y
                }
                val width = pWindow.getDouble("width", -1.0)
                val height = pWindow.getDouble("height", -1.0)
                if (width > 0 && height > 0) {
                    it.stage.width = width
                    it.stage.height = height
                }
            }

        }
        return true
    }

    fun getDockSide(side: Side): DockSide {
        return when (side) {
            Side.LEFT -> left
            Side.RIGHT -> right
            Side.TOP -> top
            Side.BOTTOM -> bottom
        }
    }

    fun getHalf(side: Side, isMain: Boolean): DockSide.Half {
        val dockSide = getDockSide(side)
        return if (isMain) dockSide.mainHalf else dockSide.otherHalf
    }

    internal fun makeDraggable(source: Node, dockable: Dockable) {

        source.onDragDetected = EventHandler { event ->
            highlightDropTargets()
            val db = source.startDragAndDrop(TransferMode.MOVE)
            val owner = findOwner(dockable)
            // Either from a Floating Dock (half==null), or from a Half which has the given dockable selected.
            val wasOpen = owner == null || owner.isVisible(dockable)
            val content = DockableDragData(dockable.dockableId, wasOpen)
            db.setContent(mapOf(DockableDragData.dataFormat to content))

            event.consume()
        }

        source.onDragDone = EventHandler { event ->
            if (event.transferMode == null) {
                findOwner(dockable)?.let { owner ->
                    owner.remove(dockable)
                    makeFloating(dockable)
                }
            }
            unhighlightDropTargets()
            event.consume()
        }
    }

    internal fun makeDroppable(target: Node, add: (Dockable, Boolean) -> Unit) {
        target.onDragOver = EventHandler { event ->
            if (event.dragboard.hasContent(DockableDragData.dataFormat)) {
                event.acceptTransferModes(TransferMode.MOVE)
                target.styleClass.addOnce("drag-over")
            }
            event.consume();
        }
        target.onDragExited = EventHandler { event ->
            target.styleClass.remove("drag-over")
            event.consume()
        }
        target.onDragDropped = EventHandler { event ->
            var success = false
            val db = event.dragboard
            if (db.hasContent(DockableDragData.dataFormat)) {
                val dragData = db.getContent(DockableDragData.dataFormat) as? DockableDragData
                if (dragData != null) {
                    find(dragData.id)?.let { (owner, dockable) ->
                        owner.remove(dockable)
                        add(dockable, dragData.wasOpen)
                    }
                    success = true
                }
            }

            event.isDropCompleted = success
            event.consume()

            // Ideally we wouldn't need this, but dragging from a Floating dock doesn't call
            // onDragDone. Moving the dockable within a "runLater" didn't help. Grr.
            unhighlightDropTargets()
        }
    }

    private fun highlightDropTargets() {
        styleClass?.addOnce("drag-dockable")
        floatings.forEach { it.highlightDropTargets() }
    }

    private fun unhighlightDropTargets() {
        styleClass?.remove("drag-dockable")
        floatings.forEach { it.unhighlightDropTargets() }
    }

    open fun makeFloating(dockable: Dockable): Floating {
        val floating = DefaultFloating(Stage(), this)
        floating.add(dockable)
        floating.stage.sizeToScene()
        return floating
    }

    companion object {
        val cssUrl: String? = Harbour::class.java.getResource("harbourfx.css")?.toExternalForm()
        val compactCssUrl: String? = Harbour::class.java.getResource("harbourfx-compact.css")?.toExternalForm()
    }
}


/**
 * The node tree is quite tricky to describe (it's probably better to look at the code), but here goes!
 *
 * [Dock]s contain [Dockable.dockableContent].
 * [DockSide] only contains buttons which toggle each [Dockable], it does NOT contain any [Dock]s.
 *
 * [borderPane] and [centerColumn] have [DockSide]s in their top/bottom/left/right / nodes.
 *
 * [leftMiddleRight] and [topMiddleBottom] have [Dock]s in their top/bottom/left/right nodes.
 *
 * So the tree looks like this :
 *
 *     this.borderPane (BorderPane)
 *         left = harbour.left (DockSide)
 *         center = this.leftMiddleRight (TripleSplitPane)
 *             left = DualSplitPane of Dock
 *             center = centerColumn (BorderPane)
 *                 top = harbour.top (DockSide)
 *                 center = this.topMiddleBottom (BorderPane)
 *                     top = DualSplitPane of Dock
 *                     center = harbour.center
 *                     bottom = DualSplitPane of Dock
 *                 bottom = harbour.bottom (DockSide)
 *             right = DualSplitPane of Dock
 *         right = harbour.right (DockSide)
 *
 */
open class HarbourSkin(harbour: Harbour) : SkinBase<Harbour>(harbour) {

    /**
     * The top-level node, which divides the main GUI into three columns.
     * Left and right are thin strips of [DockSide]s (containing toggle button for each of the [Dockable]s).
     * The center is [leftMiddleRight], which is a [SplitPane]
     */
    protected val borderPane = BorderPane()

    /**
     * The "center" child of [leftMiddleRight].
     * Top and bottom are thin strips of [DockSide]s (containing toggle button for each of the [Dockable]s).
     * The center is [topMiddleBottom], which is a [SplitPane].
     */
    protected val centerColumn = BorderPane()

    /**
     * Contains between 1 and 3 columns. The "center" column is [centerColumn], and the other two (if they exist)
     * are the [DualSplitPane] which contains zero, one or two [Dock] nodes related to
     * the currently selected [Dockable] in [DockSide.Half] of [Harbour.left] and [Harbour.right].
     */
    protected val leftMiddleRight = TripleSplitPane(centerColumn)


    /**
     * Contains between 1 and 3 rows. The "center" row is the [Harbour.center] (the main window's content).
     * The other two (if they exist) are [DualSplitPane] which contain zero, one or two [Dock] nodes related to
     * the current selected [Dockable] in [DockSide.Half] of [Harbour.top] and [Harbour.bottom].
     */
    protected val topMiddleBottom = TripleSplitPane(harbour.center)


    init {
        leftMiddleRight.firstDividerPosition().bindBidirectional(harbour.dividerPositionProperty(Side.LEFT))
        leftMiddleRight.lastDividerPosition().bindBidirectional(harbour.dividerPositionProperty(Side.RIGHT))
        topMiddleBottom.firstDividerPosition().bindBidirectional(harbour.dividerPositionProperty(Side.TOP))
        leftMiddleRight.lastDividerPosition().bindBidirectional(harbour.dividerPositionProperty(Side.BOTTOM))

        topMiddleBottom.orientation = Orientation.VERTICAL

        // Using runLater is a bit of a bodge, but I've spent so long debugging and got nowhere.
        // Without this, when harbour.resizeWithParent = false, and Dockables are added after the Stage is
        // shown, and before application's start method completes, the SplitPane ignore the split positions
        // FYI, SplitPaneSkin.setupContentAndDividerForLayout is called twice, the first time everything is fine.
        // the 2nd time "resize" = true, and getResizableWithParentArea returns 0.
        // My code isn't in the stack trace. It's only from "layout" processing.
        // Afterwards, the SplitPane's divider positions report what they *should* be, but the split is in the wrong place.
        // This is why I think the bug lies in SplitPane, and not my code.
        // I've tried making a simple test case, but couldn't replicate it with something trivial.
        // If I had infinite time and patience...
        Platform.runLater { children.add(borderPane) }

        borderPane.left = harbour.left
        borderPane.center = leftMiddleRight
        borderPane.right = harbour.right

        centerColumn.top = harbour.top
        centerColumn.center = topMiddleBottom
        centerColumn.bottom = harbour.bottom

        for (half in harbour.halves) {
            half.selectionModel.selectedItemProperty().addListener { _, _, _ -> changeContent(half) }
            changeContent(half)
        }

        // If any of the resizableWithParent values changes, change the appropriate node.
        harbour.left.resizableWithParentProperty().addListener { _, _, newValue ->
            leftMiddleRight.left?.let { TripleSplitPane.setResizableWithParent(it, newValue) }
        }
        harbour.right.resizableWithParentProperty().addListener { _, _, newValue ->
            leftMiddleRight.right?.let { TripleSplitPane.setResizableWithParent(it, newValue) }
        }
        harbour.top.resizableWithParentProperty().addListener { _, _, newValue ->
            topMiddleBottom.top?.let { TripleSplitPane.setResizableWithParent(it, newValue) }
        }
        harbour.bottom.resizableWithParentProperty().addListener { _, _, newValue ->
            topMiddleBottom.bottom?.let { TripleSplitPane.setResizableWithParent(it, newValue) }
        }
    }


    protected fun changeContent(half: DockSide.Half) {
        val splitPane = if (half.dockSide.side == Side.LEFT || half.dockSide.side == Side.RIGHT) leftMiddleRight else topMiddleBottom

        val dockable = half.selectionModel.selectedItem
        val oldDock = half.dock.value
        val newDock = dockable?.createDock(half)
        half.dock.value = newDock

        val isFirst = half.dockSide.side == Side.TOP || half.dockSide.side == Side.LEFT
        val currentNode = splitPane.getSideNode(isFirst) as? DualSplitPane

        val isMain = half.isMain()

        // Can we simply replace the existing Dock with a new one?
        if (oldDock != null && newDock != null) {
            if (currentNode == null) throw RuntimeException("Failed to replace a Docked item.")
            currentNode.setNode(isMain, newDock)
            return
        }

        // Adding a new Dock
        if (oldDock == null && newDock != null) {
            if (currentNode == null) {
                // This whole side currently has nothing showing.
                val doubleSplit = DualSplitPane()
                doubleSplit.dividerPosition().bindBidirectional(skinnable.splitPositionProperty(half.dockSide.side))


                doubleSplit.orientation = if (half.dockSide.side.isVertical) Orientation.VERTICAL else Orientation.HORIZONTAL
                doubleSplit.setNode(isMain, newDock)
                splitPane.setSideNode(isFirst, doubleSplit)

                if (!half.dockSide.resizableWithParent) {
                    TripleSplitPane.setResizableWithParent(doubleSplit, false)
                }

            } else {
                // The other half is showing, and now we need to split to show both halves.
                currentNode.setNode(isMain, newDock)
            }
            return
        }

        // Removing an existing Dock
        if (oldDock != null && newDock == null) {
            if (currentNode == null) throw RuntimeException("Failed to remove a Docked item.")
            currentNode.setNode(isMain, null)
            if (currentNode.isEmpty()) {
                splitPane.setSideNode(isFirst, null)
            }
            return
        }

    }

}


fun <T> MutableList<T>.addOnce(item: T) {
    if (!this.contains(item)) this.add(item)
}

fun <T> ObservableValue<T>.addOneTimeListener(action: (ObservableValue<out T>, T, T) -> Unit) {
    this.addListener(
            object : ChangeListener<T> {
                override fun changed(observable: ObservableValue<out T>, oldValue: T, newValue: T) {
                    action(observable, oldValue, newValue)
                    this@addOneTimeListener.removeListener(this)
                }
            })
}
