/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.beans.binding.Bindings
import javafx.beans.property.*
import javafx.beans.value.ChangeListener
import javafx.collections.ListChangeListener
import javafx.geometry.Orientation
import javafx.scene.Node
import javafx.scene.control.Control
import javafx.scene.control.SkinBase
import javafx.scene.control.SplitPane

/**
 * A split pane, which can have zero, one or two panes.
 * The divider position is remembered, so removing and then re-adding a node will keep the divider in
 * the same place.
 *
 * Unlike a regular [SplitPane] which has a list of item nodes, this has a [left] and [right] node.
 *
 * This is implemented using a regular [SplitPane] within [DualSplitPaneSkin].
 *
 * [Harbour] uses these on each of the four edges [left] and/or [right] contain a [Dock].
 */
class DualSplitPane : Control() {

    private val leftProperty = SimpleObjectProperty<Node?>(null)
    fun leftProperty(): ObjectProperty<Node?> = leftProperty

    var left: Node?
        get() = leftProperty.value
        set(v) {
            leftProperty.value = v
        }

    private val rightProperty = SimpleObjectProperty<Node?>(null)
    fun rightProperty(): ObjectProperty<Node?> = rightProperty
    var right: Node?
        get() = rightProperty.value
        set(v) {
            rightProperty.value = v
        }

    /**
     * An alias for [leftProperty]
     */
    fun topProperty() = leftProperty()

    /**
     * An alias for [rightProperty]
     */
    fun bottomProperty() = rightProperty()

    /**
     * An alias for [left] - useful when orientation is VERTICAL
     */
    var top: Node?
        get() = left
        set(v) {
            left = v
        }

    /**
     * An alias for [right] - useful when orientation is VERTICAL
     */
    var bottom: Node?
        get() = right
        set(v) {
            right = v
        }

    private val dividerPositionProperty = SimpleDoubleProperty(0.5)
    fun dividerPosition(): DoubleProperty = dividerPositionProperty
    var dividerPosition: Double
        get() = dividerPositionProperty.value
        set(v) {
            dividerPositionProperty.value = v
        }

    private val orientationProperty = SimpleObjectProperty<Orientation>(Orientation.HORIZONTAL)
    fun orientationProperty(): ObjectProperty<Orientation> = orientationProperty
    var orientation: Orientation
        get() = orientationProperty.value
        set(v) {
            orientationProperty.value = v
        }

    internal val itemCountProperty = SimpleIntegerProperty(0)
    fun itemCount(): ReadOnlyIntegerProperty = itemCountProperty
    val itemCount: Int
        get() = itemCount().value.toInt()


    init {
        styleClass.add("dual-split-pane")
    }

    override fun createDefaultSkin() = DualSplitPaneSkin(this)


    fun setNode(isFirst: Boolean, node: Node?) {
        if (isFirst) {
            left = node
        } else {
            right = node
        }
    }

    fun isEmpty() = left == null && right == null

    fun getNode(isFirst: Boolean) = if (isFirst) left else right

    companion object {

        fun setResizableWithParent(node: Node, value: Boolean) {
            SplitPane.setResizableWithParent(node, value)
        }

        fun isResizableWithParent(node: Node): Boolean = SplitPane.isResizableWithParent(node)
    }

}

class DualSplitPaneSkin(control: DualSplitPane)

    : SkinBase<DualSplitPane>(control) {

    private val splitPane = SplitPane()

    private val divPosListener = ChangeListener<Number> { _, _, newValue ->
        control.dividerPosition = newValue.toDouble()
    }

    init {
        splitPane.dividers.addListener { c: ListChangeListener.Change<out SplitPane.Divider> ->
            while (c.next()) {
                c.addedSubList.forEach { it.positionProperty().addListener(divPosListener) }
                c.removed.forEach { it.positionProperty().removeListener(divPosListener) }
            }
        }

        children.add(splitPane)
        control.left?.let { splitPane.items.add(it) }
        control.right?.let { splitPane.items.add(it) }
        if (splitPane.dividerPositions.isNotEmpty()) {
            splitPane.setDividerPosition(0, control.dividerPosition)
        }
        control.dividerPosition().addListener { _, _, newValue ->
            if (splitPane.dividerPositions.isNotEmpty()) {
                splitPane.setDividerPosition(0, newValue.toDouble())
            }
        }
        control.itemCountProperty.bind(Bindings.size(splitPane.items))
        splitPane.orientationProperty().bind(control.orientationProperty())

        control.leftProperty().addListener { _, oldValue, newValue ->
            val div = control.dividerPosition
            if (oldValue != null) splitPane.items.remove(oldValue)
            if (newValue != null) {
                splitPane.items.add(0, newValue)
                splitPane.setDividerPosition(0, div)
            }
        }

        control.rightProperty().addListener { _, oldValue, newValue ->
            val div = control.dividerPosition
            if (oldValue != null) splitPane.items.remove(oldValue)
            if (newValue != null) {
                splitPane.items.add(newValue)
                splitPane.setDividerPosition(0, div)
            }
        }


    }

}
