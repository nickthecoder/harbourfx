/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx.demo

import javafx.application.Application
import javafx.beans.property.SimpleObjectProperty
import javafx.event.EventHandler
import javafx.geometry.Side
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.ContextMenu
import javafx.scene.control.Label
import javafx.scene.control.MenuItem
import javafx.scene.control.TextArea
import javafx.stage.Stage
import uk.co.nickthecoder.harbourfx.AbstractDockable
import uk.co.nickthecoder.harbourfx.Dockable
import uk.co.nickthecoder.harbourfx.DockableFactory
import uk.co.nickthecoder.harbourfx.Harbour
import java.util.prefs.Preferences

class Demo : Application() {

    override fun start(primaryStage: Stage) {

        val mainWindow = MainWindow(primaryStage)

        primaryStage.show()

        // Optional...
        // Loads preferences from the registry (if they exist).
        if (!mainWindow.harbour.load(preferences())) {
            mainWindow.createDefaultDocks()
        }

        // Optional...
        // Saves the state when the primaryStage is closed.
        primaryStage.onHiding = EventHandler { mainWindow.harbour.save(preferences()) }
    }

    /**
     * Where the registry the harbour preferences will be saved.
     */
    private fun preferences(): Preferences = Preferences.userNodeForPackage(Demo::class.java)

}

class MainWindow(stage: Stage) {

    val mainContent = TextArea(
        """HarbourFX

This is in place of your application's main content.

Try dragging the docked items (which are on the left by default).
You can drag them to another edge, or elsewhere to make them float.

Each side of the Harbour has two places to dock.

"""
    )

    val harbour = Harbour(mainContent, DemoDockableFactory())
    // If you don't want to use the default save/load feature of HarbourFX, you may use the
    // NullDockableFactory instead.

    init {
        // Optional...
        // Prevent any of the Dockables being floated and then closed forever!
        // Closing a Floating puts all of its Dockables in to the left main DockSide Half.
        harbour.allowClose = false

        stage.scene = Scene(harbour, 700.0, 500.0)

        stage.scene.stylesheets.add(Harbour.compactCssUrl)
        // Or use the "default" version, which uses normal looking buttons, which take up more space
        //stage.scene.stylesheets.add(Harbour.cssUrl)

        // Optional...
        // The bottom will resize with the parent, other sides will remain bound to harbour's value.
        harbour.bottom.resizableWithParentProperty().unbind()
        harbour.bottom.resizableWithParent = true
    }

    /**
     * The first time that the demo is run, it will not find previously saved preferences,
     * so dockables will be added in default places.
     * The next time you run the application, this will NOT get called, and instead
     * the dockables will be restored to where they were on the previous run.
     */
    fun createDefaultDocks() {
        // Here's four ways to add a dockable to the harbour
        harbour.show("HELLO")
        //harbour.addDockable("HELLO", Side.LEFT, isMain = true, show = false)
        harbour.left.mainHalf.dockables.add(harbour.factory.createDockable("WELCOME")!!)
        harbour.addDockable(harbour.factory.createDockable("FOO")!!, Side.LEFT, isMain = false, show = false)
    }

}

/**
 * An example factory class, used when loading state of the harbour when the demo
 * starts. If you do not want to use HarbourFX's save/load feature, then you
 * do not need a DockableFactory.
 */
class DemoDockableFactory : DockableFactory {
    override fun createDockable(dockableId: String): Dockable? {
        return when (dockableId) {
            "HELLO" -> DemoDockable("HELLO", "Hello", "Hello World")
            "WELCOME" -> DemoDockable("WELCOME", "Welcome", "Welcome to the Harbour\nNeed a place to dock?")
            "FOO" -> DemoDockable("FOO", "Foo", "Foo")
            else -> null
        }
    }
}

/**
 * A simple Dockable, containing a single Label.
 */
class DemoDockable(id: String, title: String, content: String)

    : AbstractDockable(id) {

    // Replace Label with the main node for your dockable.
    override val dockableContent = SimpleObjectProperty<Node>(Label(content))

    override fun settingsMenu(): ContextMenu {
        return ContextMenu().apply {
            items.add(MenuItem("Hello"))
        }
    }

    init {
        this.dockableTitle.value = title
    }
}

fun main(vararg args: String) {
    Application.launch(Demo::class.java, * args)
}