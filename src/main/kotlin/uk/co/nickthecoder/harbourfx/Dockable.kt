/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.beans.property.BooleanProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.scene.Node
import javafx.scene.control.ContextMenu
import javafx.scene.control.MenuButton
import javafx.scene.image.Image

interface Dockable {

    /**
     * A unique identifier for the Dockable.
     */
    val dockableId: String

    /**
     * An option icon to appear in the [DockSide].
     */
    val dockableIcon: Image?

    /**
     * The title of the Dockable. Used in the [DockSide] and the [Dock].
     */
    val dockableTitle: StringProperty

    /**
     * An optional prefix to the [dockableTitle], only displayed in the [DockSide].
     * I use this to display a shortcut key. e.g. "1 " when the shortcut ctrl+1
     * will toggle this Dockable.
     */
    val dockablePrefix: StringProperty

    /**
     * The main content node.
     */
    val dockableContent: ObjectProperty<Node>

    val visibleProperty: BooleanProperty

    /**
     * Simple [Dockable] return a Dock, whereas [TabbedDockable]s
     * return a TabbedDock. Feel free to create other special types of Dock.
     */
    fun createDock(half: DockSide.Half): Dock = Dock(half, this)

    /**
     * When not null, the [Dock] will contain a [MenuButton] with the menu's items.
     */
    fun settingsMenu(): ContextMenu? = null

}

/**
 * Takes care of some boilerplate code.
 * Nothing particularly interesting to see here!
 */
abstract class AbstractDockable(
    override val dockableId: String,
    override val dockableIcon: Image? = null
)

    : Dockable {

    override val dockableTitle = SimpleStringProperty("")

    override val dockablePrefix = SimpleStringProperty("")

    override val visibleProperty = SimpleBooleanProperty(false)

}
