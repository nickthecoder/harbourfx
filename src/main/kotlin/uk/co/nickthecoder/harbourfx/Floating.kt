/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.ScrollPane
import javafx.scene.layout.Pane
import javafx.scene.layout.VBox
import javafx.stage.Stage

/**
 * Note. Expect this interface to change in subsequent versions of HarbourFX.
 * (because I know it isn't very good!)
 *
 * I don't think this should be a problem though, because I expect most people to use DefaultFloating,
 * and will not interact directly with it.
 */
interface Floating : DockOwner {

    val stage: Stage

    fun docks(): Collection<Dock>

    override fun isVisible(dockable: Dockable) = docks().firstOrNull { it.dockable === dockable } != null


    fun highlightDropTargets()

    fun unhighlightDropTargets()

    fun add(dockable: Dockable)

}

/**
 * Contains a set of [Dock]s arranged inside a [VBox], surrounded by a single [ScrollPane].
 */
open class DefaultFloating(final override val stage: Stage, final override val harbour: Harbour)

    : Floating {

    private val vBox = VBox()

    private val scroll = ScrollPane(vBox)


    init {
        scroll.styleClass.add("dock-floating")
        scroll.isFitToWidth = true

        vBox.styleClass.add("dock-floating-box")
        vBox.children.add(spacer())

        // We need to set the initOwner AND show the the floating stage.
        // However, if the harbour's stage isn't showing yet, then initOwner won't work correctly. Grr.
        // So, we must wait until the harbour's stage is showing before showing this one.
        if (harbour.scene.window.isShowing) {
            stage.initOwner(harbour.scene.window)
            stage.show()
        } else {
            harbour.scene.window.showingProperty().addOneTimeListener { _, _, _ ->
                stage.initOwner(harbour.scene.window)
                stage.show()
            }
        }
        stage.scene = Scene(scroll)
        vBox.isFillWidth = true
        harbour.floatings.add(this)
        stage.onHidden = EventHandler {
            harbour.floatings.remove(this)
        }

        stage.scene.stylesheets.addOnce(Harbour.cssUrl)
    }


    override fun docks(): Collection<Dock> = vBox.children.filterIsInstance<Dock>()


    override fun show(dockable: Dockable) {
        if (!isVisible(dockable)) {
            add(dockable)
        }
    }

    override fun hide(dockable: Dockable) {}

    override fun toggle(dockable: Dockable) {}

    private fun spacer(): Pane {
        val pane = Pane()
        pane.styleClass.add("spacer")
        harbour.makeDroppable(pane) { dockable, _ ->
            var index = vBox.children.indexOf(pane)
            if (index < 0) index = 0
            val dock = Dock(this, dockable)
            vBox.children.add(index, dock)
            vBox.children.add(index, spacer())
            updateTitle()
        }
        return pane
    }

    override fun add(dockable: Dockable) {
        val dock = Dock(this, dockable)
        vBox.children.addAll(dock, spacer())
        updateTitle()
        dockable.visibleProperty.value = true
    }


    override fun remove(dockable: Dockable) {
        docks().firstOrNull { it.dockable === dockable }?.let { dock ->
            val index = vBox.children.indexOf(dock)
            if (index >= 0) {
                vBox.children.removeAt(index)
                vBox.children.removeAt(index) // Remove the spacer
            }
            if (docks().isEmpty()) {
                stage.hide()
            } else {
                updateTitle()
            }
            dockable.visibleProperty.value = false
        }
    }

    protected open fun updateTitle() {
        if (stage.isShowing) {
            stage.title = docks().joinToString(separator = ", ") { it.dockable.dockableTitle.value }
        }
    }

    override fun highlightDropTargets() {
        scroll.styleClass.addOnce("drag-dockable")
    }

    override fun unhighlightDropTargets() {
        scroll.styleClass.remove("drag-dockable")
    }

}
