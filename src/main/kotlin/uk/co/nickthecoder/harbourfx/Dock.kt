/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority

/**
 * A Dock has a [ToolBar] at the top, plus the content [Node] from [Dockable.dockableContent].
 *
 * Docks are created and destroyed whenever a [Dockable] is selected from one of the buttons
 * in [DockSide].
 *
 */
open class Dock(val dockOwner: DockOwner, val dockable: Dockable) : Control() {

    init {
        styleClass.add("dock")
    }

    override fun createDefaultSkin(): Skin<*> = DockSkin(this)

}

open class DockSkin<C : Dock>(control: C) : SkinBase<C>(control) {

    protected val borderPane = BorderPane()

    protected val title = Label()

    protected val titleBar = ToolBar()

    init {
        children.add(borderPane)

        title.textProperty().bind(skinnable.dockable.dockableTitle)
        skinnable.dockable.dockableIcon?.let {
            titleBar.items.add(ImageView(it))
        }

        val spacer = HBox()
        HBox.setHgrow(spacer, Priority.ALWAYS)

        titleBar.items.addAll(title, spacer)

        skinnable.dockable.settingsMenu()?.let { menu ->
            val settings = MenuButton("⏣")
            settings.items.addAll(menu.items)
            titleBar.items.add(settings)
        }

        val minimise = Button("⋎")
        val maximise = Button("⋏")

        minimise.onAction = EventHandler {
            skinnable.dockOwner.hide(skinnable.dockable)
            borderPane.center = null
            titleBar.items.add(titleBar.items.indexOf(minimise), maximise)
            titleBar.items.remove(minimise)
        }
        maximise.onAction = EventHandler {
            borderPane.center = skinnable.dockable.dockableContent.value
            titleBar.items.add(titleBar.items.indexOf(maximise), minimise)
            titleBar.items.remove(maximise)
        }

        titleBar.items.add(minimise)
        borderPane.top = titleBar
        borderPane.center = skinnable.dockable.dockableContent.value

        skinnable.dockable.dockableContent.addListener { _, _, newValue -> borderPane.center = newValue }

        skinnable.dockOwner.harbour.makeDraggable(titleBar, skinnable.dockable)
    }

}
