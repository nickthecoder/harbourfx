/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.beans.property.BooleanProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.geometry.Side
import javafx.scene.Group
import javafx.scene.control.*
import javafx.scene.image.ImageView
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox

class DockSide(val harbour: Harbour, val side: Side)

    : Control() {

    val mainHalf = Half()

    val otherHalf = Half()

    /**
     * When true, if the whole [Harbour] is resized, then this DockSide is also resized.
     * The default is for this property to be bound to [Harbour.resizableWithParentProperty] (whose default is false).
     */
    var resizableWithParent: Boolean
        get() = resizableWithParentProperty.value
        set(v) {
            resizableWithParentProperty.value = v
        }
    private val resizableWithParentProperty = SimpleBooleanProperty(false)
    fun resizableWithParentProperty(): BooleanProperty = resizableWithParentProperty


    init {
        resizableWithParentProperty.bind(harbour.resizableWithParentProperty())
        styleClass.addAll("dock-side", "dock-side-${side.name.toLowerCase()}")
    }


    override fun createDefaultSkin() = DockSideSkin(this)


    inner class Half : DockOwner {

        override val harbour: Harbour
            get() = this@DockSide.harbour

        val dockSide = this@DockSide

        val dockables: ObservableList<Dockable> = FXCollections.observableArrayList()

        val selectionModel: SingleSelectionModel<Dockable?> = DockSideSelectionModel().apply {
            selectedItemProperty().addListener { _, oldValue, newValue ->
                oldValue?.visibleProperty?.value = false
                newValue?.visibleProperty?.value = true
            }
        }

        internal val dock: ObjectProperty<Dock?> = SimpleObjectProperty<Dock?>(null)

        override fun isVisible(dockable: Dockable) = selectionModel.selectedItem === dockable

        override fun hide(dockable: Dockable) {
            if (selectionModel.selectedItem === dockable) {
                selectionModel.clearSelection()
            }
        }

        override fun remove(dockable: Dockable) {
            dockables.remove(dockable)
        }

        fun isMain(): Boolean = mainHalf === this

        override fun toggle(dockable: Dockable) {
            if (dockables.contains(dockable)) {
                if (selectionModel.selectedItem === dockable) {
                    selectionModel.select(null)
                } else {
                    selectionModel.select(dockable)
                }
            }
        }

        override fun show(dockable: Dockable) {
            if (!dockables.contains(dockable)) {
                harbour.findOwner(dockable)?.remove(dockable)
                dockables.add(dockable)
            }
            selectionModel.select(dockable)
        }

        inner class DockSideSelectionModel : SingleSelectionModel<Dockable?>(), ListChangeListener<Dockable> {

            init {
                dockables.addListener(this)
            }

            override fun select(obj: Dockable?) {
                if (obj == null || dockables.contains(obj)) super.select(obj)
            }

            override fun onChanged(c: ListChangeListener.Change<out Dockable>) {
                while (c.next()) {
                    c.removed.forEach { if (it === selectedItem) clearSelection() }
                }
            }

            override fun getItemCount(): Int = dockables.size
            override fun getModelItem(index: Int): Dockable? =
                if (index < 0 || index >= dockables.size) null else dockables[index]
        }
    }

}

class DockSideSkin(dockSide: DockSide) : SkinBase<DockSide>(dockSide) {

    private val mainHalfSkin = HalfSkin(skinnable.mainHalf)

    private val otherHalfSkin = HalfSkin(skinnable.otherHalf)

    private val both: Pane = if (skinnable.side.isVertical) VBox() else HBox()

    init {
        both.styleClass.add("dock-side")
        both.styleClass.add("dock-side-${skinnable.side.name.toLowerCase()}")

        children.add(both)
        both.children.addAll(mainHalfSkin.buttons, mainHalfSkin.spacer, otherHalfSkin.spacer, otherHalfSkin.buttons)
    }

    inner class HalfSkin(val half: DockSide.Half) {

        internal val buttons: Pane = if (skinnable.side.isVertical) VBox() else HBox()

        internal val spacer = Pane()

        private val toggleGroup = ToggleGroup()

        init {

            buttons.styleClass.add("dock-side-half")
            if (half.isMain()) {
                buttons.styleClass.add("dock-side-half-main")
            } else {
                buttons.styleClass.add("dock-side-half-other")
            }
            spacer.styleClass.add("spacer")

            if (skinnable.side.isVertical) {
                VBox.setVgrow(spacer, Priority.SOMETIMES)
            } else {
                HBox.setHgrow(spacer, Priority.SOMETIMES)
            }

            half.dockables.addListener { c: ListChangeListener.Change<out Dockable> ->
                while (c.next()) {
                    c.removed.forEach { removeDockable(c.from) }
                    c.addedSubList.forEach { addDockable(it) }
                }
            }

            half.selectionModel.selectedItemProperty().addListener { _, _, newValue ->
                if (newValue == null) {
                    toggleGroup.selectToggle(null)
                } else {
                    findButton(newValue)?.isSelected = true
                }
            }

            half.dockables.forEach { addDockable(it) }

            skinnable.harbour.makeDroppable(spacer) { newDockable, wasOpen ->
                if (half.isMain()) {
                    half.dockables.add(newDockable)
                } else {
                    half.dockables.add(0, newDockable)
                }
                if (wasOpen) {
                    half.selectionModel.select(newDockable)
                }
            }

        }

        private fun removeDockable(index: Int) {
            buttons.children.removeAt(index)
        }

        private fun addDockable(dockable: Dockable) {
            val index = half.dockables.indexOf(dockable)
            val button = DockableButton(dockable)
            if (skinnable.side.isVertical) {
                button.rotate = -90.0
                buttons.children.add(index, Group(button))
            } else {
                buttons.children.add(index, button)
            }
        }

        private fun findButton(dockable: Dockable): DockableButton? {
            buttons.children.forEach { node ->
                if ((node as? DockableButton)?.dockable === dockable) return node
                if (node is Group) {
                    val ungrouped = node.children.firstOrNull() as? DockableButton
                    if (ungrouped?.dockable == dockable) return ungrouped
                }
            }
            return null
        }


        private inner class DockableButton(val dockable: Dockable) : ToggleButton() {

            init {
                styleClass.add("dock-side-button")

                toggleGroup = this@HalfSkin.toggleGroup
                textProperty().bind(dockable.dockablePrefix.concat(dockable.dockableTitle))
                dockable.dockableIcon?.let { graphic = ImageView(it) }

                onAction = EventHandler { half.toggle(dockable) }

                skinnable.harbour.makeDraggable(this, dockable)
                //dockable.settingsMenu()?.let { menu ->
                //    contextMenu = ContextMenu().apply { items.addAll(menu.items) }
                //}

                skinnable.harbour.makeDroppable(this) { newDockable, wasOpen ->
                    val index = Math.max(0, half.dockables.indexOf(dockable)) + if (half.isMain()) 0 else 1

                    half.dockables.add(index, newDockable)
                    if (wasOpen) {
                        half.selectionModel.select(newDockable)
                    }
                }

                // Show the dockable when dragging over this button, so that the target of the drop
                // can be something inside the Dockable.
                onDragEntered = EventHandler { event ->
                    // Don't open when we are dragging Dockable around though!
                    if (!event.dragboard.hasContent(DockableDragData.dataFormat)) {
                        half.show(dockable)
                    }
                }
            }

        }
    }
}
