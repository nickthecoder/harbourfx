/*
HarbourFX Copyright (C) 2019  Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.harbourfx

import javafx.collections.ListChangeListener
import javafx.event.EventHandler
import javafx.scene.control.*

/**
 * A [Dock] for a [TabbedDockable].
 *
 * The [Tab]s are displayed in the ToolBar, and therefore uses less space compared to a regular TabPane.
 */
open class TabbedDock(half: DockSide.Half, val tabbedDockable: TabbedDockable)
    : Dock(half, tabbedDockable) {

    init {
        styleClass.add("tabbed-dock")
    }

    override fun createDefaultSkin(): Skin<*> = TabbedDockSkin(this)

}

open class TabbedDockSkin<C : TabbedDock>(control: C) : DockSkin<C>(control) {

    private val tabsStartIndex = titleBar.items.indexOf(title) + 2
    private var tabsVisible = false
    private val toggleGroup = ToggleGroup()

    init {
        titleBar.items.add(tabsStartIndex - 1, Separator())

        control.tabbedDockable.tabs.addListener { c: ListChangeListener.Change<out Tab> ->
            val tabs = control.tabbedDockable.tabs
            while (c.next()) {
                if (tabsVisible) {
                    c.addedSubList.forEachIndexed { i, tab -> addTab(c.from + i, tab) }
                } else {
                    if (tabs.size > 1) {
                        tabs.forEachIndexed { i, tab -> addTab(i, tab) }
                        tabsVisible = true
                    }
                }
                c.removed.forEach { removeTab(it) }
            }
            if (tabs.size < 2) {
                tabs.forEach { tab -> removeTab(tab) }
                tabsVisible = false
            }
        }
        if (control.tabbedDockable.tabs.size >= 2) {
            control.tabbedDockable.tabs.forEachIndexed { i, tab -> addTab(i, tab) }
            tabsVisible = true
        }

        control.tabbedDockable.selectionModel.selectedItemProperty().addListener { _, _, currentTab ->
            for (node in titleBar.items) {
                if (node is TabbedDockSkin<*>.TabToggleButton && node.tab === currentTab) {
                    node.isSelected = true
                }
            }
        }
    }

    private fun findTabButton(tab: Tab?): TabToggleButton? {
        return titleBar.items.filterIsInstance<TabToggleButton>().firstOrNull { it.tab === tab }
    }

    private fun addTab(index: Int, tab: Tab) {
        val ttb = TabToggleButton(tab)
        if (skinnable.tabbedDockable.selectionModel.selectedItem === tab) {
            ttb.isSelected = true
        }
        titleBar.items.add(tabsStartIndex + index, ttb)
    }

    private fun removeTab(tab: Tab) {
        titleBar.items.remove(findTabButton(tab))
    }

    inner class TabToggleButton(val tab: Tab) : ToggleButton(tab.text, tab.graphic) {

        init {
            toggleGroup = this@TabbedDockSkin.toggleGroup
            onAction = EventHandler { skinnable.tabbedDockable.selectionModel.select(tab) }
        }
    }
}
