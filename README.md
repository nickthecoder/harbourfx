HarbourFx
=========

A docking system for JavaFX applications.
It's written in Kotlin, but you can use it from within a Java application.

![Demo](./screenshots/demo1.png)

[Other Screenshots](screenshots)

Usage
-----

Look at the source code for the [Demo](https://gitlab.com/nickthecoder/harbourfx/blob/master/src/main/kotlin/uk/co/nickthecoder/harbourfx/demo/Demo.kt)

Most of it is boiler plate (to start the application and create the Stage etc).

Create the Harbour, passing the content node, and a factory class.
Add this to your scene.

    new Harbour(mainContent, new DemoDockableFactory())

Pseudo code for the factory :

    public class DemoDockableFactory() {
        public Dockable createDockable( String dockableId ) {
            if ( dockableId == "HELLO" ) return new DemoDockable("HELLO","Hello", "Hello World");
            // More ...
            return null
        }
    }

Here's the DemoDockable implementation (in Kotlin).

    class DemoDockable(id: String, title: String, content: String)

        : AbstractDockable(id) {

        // Replace Label with the main node for your dockable.
        override val dockableContent = SimpleObjectProperty<Node>(Label(content))

        init {
            this.dockableTitle.value = title
        }
    }

Add a Dockable to the harbour :

    harbour.addDockable("HELLO", Side.LEFT, isMain = true, show = false)

The positions and visibility of each of the Dockables can be saved/loaded using
Harbour.save() and Harbour.load() methods.
This uses Java's Preferences class.

Or alternatively write your own persistence code using XML or JSON etc.

Build
-----

The prerequisites are a Java JDK (including javaFX) and gradle v2.0 or higher.

For Debian Linux (as root) :

    apt-get install openjdk-8-jdk libopenjfx-java gradle

To download and build :    
    
    git clone https://gitlab.com/nickthecoder/harbourfx.git
    cd harbourfx
    gradle

