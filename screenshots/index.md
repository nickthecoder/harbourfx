Tickle
------

Here's a real-world example of HarbourFX in action from the
[Tickle](https://gitlab.com/nickthecoder/tickle) game engine.

![Tickle](tickle.png)

The left side's "main" half has a single Dockable called "Resources"
The "other" half has many Dockables, none of which are selected.

The bottom has a single Dockable called "Log".

The right has one Dockable in the "main" half and two in the "other" half.

In this application the MenuButton with the ship icon in the top right lists all of the dockable items.

The buttons which start with a digit can be toggled using Alt+n
where n is the digit for that particular dockable.
This is what Dockable.dockablePrefix is for. The prefix is only displayed in
the button's text, not the dock's title.

ToolApp
-------

Another real-world example from my
[ToolApp](https://gitlab.com/nickthecoder/toolapp) application.

![ToolApp](toolapp.png)

In this example, on the right we see the use of a TabbedDockable.
It has two tabs, "src" and "dodgeem".

There is also an extra item in the Dock's title :
A "settings" MenuButton.

The three buttons along the bottom (with Git icons) are Dockables.
Everything else is unrelated to HarbourFX.

Demo
----
Here's the demo application.
Note that the buttons are more compact than above.
Pick whichever you prefer - choose which style sheet to use.
Either 'Harbour.cssUrl' or 'Harbour.compactCssUrl'

![Demo](demo1.png)

To keep the demo short, there are no icons of prefixes for the Dockables.
